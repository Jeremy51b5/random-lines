#random-lines.py

Utilizing Python and the Turtle Graphics library this program:

	- Finds the length and width of the canvas
	
	- Sets the background to a random color.
	
	- Sets the 'turtle-pen' color.
	
	- Finds a random location on the canvas given 2 random x,y values. 
	  These values are appended to a list to ensure each coordinate is unique.
	
	- A recursion error will happen at ~2000 recalls of main(). 
	  This is used as a stoping point to take a screenshot. 
	
	- Finally, all list values are reset (exept colors) and 2 more random colors are chosen.

Dependencies:

	- Python 3+ https://www.python.org/ftp/python/3.10.4/Python-3.10.4.tar.xz
	- Python Turtle https://docs.python.org/3/library/turtle.html

Installation:

	- clone the repository > git clone https://gitlab.com/Jeremy51b5/random-lines.git
	- Install Turtle using pip > pip install turtle
	- Run the python program located in the cloned repository > python random-lines.py

Usage:

	- Create simple wallpapers. Using other scripts, pictures can be updated dynamically.
	- Using Twitters api, these pictures can be pushed to update your profile banner (see twitter-api python script: https://gitlab.com/Jeremy51b5/update-twitter-banner)

Please feel free to edit and mess around with the code!


Additional Infromation:
	
	- Created for CS-123 Python Programming Spring 2022
	- Author: Jeremy - jeremy51b5@pm.me 
	- Check out my blog! https://www.hackerchat.net
