import turtle    # Canvas/Drawing Library (See: https://docs.python.org/3/library/turtle.html)
import random    # Generate pseudo random values.
import time      # Adds a sleep function
import sys       # Access to Read/write files on the OS
import pyautogui # ScreenShot Utility lib

#gets recursion error around 5000 itterations. this method sets the default recursion limit.
sys.setrecursionlimit(2000)

#Global Vars
#Gets the canvas height and width
canvasHeight = turtle.window_height()
canvasWidth = turtle.window_width()
negativeCanvasHeight = canvasHeight * -1
negativeCanvasWidth = canvasWidth * -1

#Stores the last x and y values of turtle.
turtle_XList = []
turtle_YList = []

#stores used colors
my_ColorList = []
my_BgColorList = []

#Keeps Track of 2 current rbg colors (for screenshot title)
currentLineColor = ''
currentBgColor = ''

#Resets values of all Lists and clears the canvas.
def clearCache():
    turtle_XList.clear()
    turtle_YList.clear()
    turtle.clear()

#Change background color to a random color.
def changeBgColor():
    global currentBgColor
    alreadyFoundThatColor = False
    randomBgColor = ["#"+''.join([random.choice('0123456789ABCDEF') for j in range(6)])]
    for a in my_ColorList:
        if(a == randomBgColor):
            alreadyFoundThatColor = True
            break
    if alreadyFoundThatColor == True:
        changeBgColor()
    elif alreadyFoundThatColor == False:
        my_BgColorList.append(str(randomBgColor[0]))
        turtle.bgcolor(randomBgColor[0])
        currentBgColor = str(randomBgColor[0])
    else:
        pass


#Gets a random color that has not been previously generated
def changeLineColor():
    global currentLineColor
    alreadyFoundThatColor = False
    randomLineColor = ["#"+''.join([random.choice('0123456789ABCDEF') for j in range(6)])]
    for a in my_ColorList:
        if(a == randomLineColor):
            alreadyFoundThatColor = True
            break
    if alreadyFoundThatColor == True:
        changeLineColor()
    elif alreadyFoundThatColor == False:
        my_ColorList.append(str(randomLineColor[0]))
        turtle.color(randomLineColor[0])
        currentLineColor = str(randomLineColor[0])

    else:
        pass

#Main Function, finds a random X Y value that is within range of the canvas height and width
def main():
    #two variables to keep tack if the X Y postion already exists in the lists.
    alreadyFoundThatXPosition = False
    alreadyFoundThatYPosition = False
    #get two random x y positons
    randomXPos = random.randrange(negativeCanvasWidth,canvasWidth)
    randomYPos = random.randrange(negativeCanvasHeight,canvasHeight)

    #checks the turtle_XList to see if the value already exists in list.
    #if the randomXPos value exists in the list than it sets alreadyFoundThatXPosition to True
    for i in turtle_XList:
        if(i == randomXPos):
            alreadyFoundThatXPosition = True
            break
    #checks the turtle_YList to see if the value already exists in list.
    #if the randomXPos value exists in the list than it sets alreadyFoundThatXPosition to True
    for x in turtle_YList:
        if(x == randomYPos):
            alreadyFoundThatYPosition = True
            break
    #if the X Y position is unique than its valid
    if alreadyFoundThatXPosition == False and alreadyFoundThatYPosition == False:
        turtle_XList.append(randomXPos)
        turtle_YList.append(randomYPos)
        turtle.goto(randomXPos,randomYPos)
    #else, if the X or Y position is True than recall main() and find another set of random x y.
    elif alreadyFoundThatXPosition == True or alreadyFoundThatYPosition == True:
        main()
    else:
        pass

#Uses the PyAutoGUI lib to take a screenshot of the canvas. (The region values _may_ need to be adjusted depending on the monitor used.)
#PyAutoGUI Docs: https://pyautogui.readthedocs.io/en/latest/screenshot.html
def screenShot():
    global currentBgColor
    global currentLineColor
    myScreen = pyautogui.screenshot(region=(15,50, 1300, 800))
    myScreen.save("/home/pi/Desktop/ScreenShots/%s-%s.png"%(currentLineColor,currentBgColor))


#Start of Program
if __name__ == "__main__":
    turtle.speed(0)
    changeBgColor()
    changeLineColor()
    turtle.hideturtle()
    while True:
        try:
            main()
        except RecursionError:
                print("%s,%s"%(currentLineColor, currentBgColor))
                screenShot()
                clearCache()
                changeLineColor()
                changeBgColor()
                main()
